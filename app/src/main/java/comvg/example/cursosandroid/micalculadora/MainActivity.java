package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtNum1, txtNum2;
    private Button btnSumar, btnRestar, btnMultiplicar, btnDividir, btnLimpiar, btnCerrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);
        btnSumar = (Button) findViewById(R.id.btnSuma);
        btnRestar = (Button) findViewById(R.id.btnResta);
        btnMultiplicar = (Button) findViewById(R.id.btnMult);
        btnDividir = (Button) findViewById(R.id.btnDivi);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);

        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String num1 = txtNum1.getText().toString();
                String num2 = txtNum2.getText().toString();
                if(num1.trim().matches("") || num2.trim().matches(""))
                    Toast.makeText(getApplicationContext(), "Llena ambos campos", Toast.LENGTH_SHORT).show();
                else {
                    Operaciones operacion = new Operaciones(Float.valueOf(num1), Float.valueOf(num2));
                    Toast.makeText(getApplicationContext(), "Resultado: " + String.valueOf(operacion.suma()), Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String num1 = txtNum1.getText().toString();
                String num2 = txtNum2.getText().toString();
                if(num1.trim().matches("") || num2.trim().matches(""))
                    Toast.makeText(getApplicationContext(), "Llena ambos campos", Toast.LENGTH_SHORT).show();
                else {
                    Operaciones operacion = new Operaciones(Float.valueOf(num1), Float.valueOf(num2));
                    Toast.makeText(getApplicationContext(), "Resultado: " + String.valueOf(operacion.resta()), Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnMultiplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String num1 = txtNum1.getText().toString();
                String num2 = txtNum2.getText().toString();
                if(num1.trim().matches("") || num2.trim().matches(""))
                    Toast.makeText(getApplicationContext(), "Llena ambos campos", Toast.LENGTH_SHORT).show();
                else {
                    Operaciones operacion = new Operaciones(Float.valueOf(num1), Float.valueOf(num2));
                    Toast.makeText(getApplicationContext(), "Resultado: " + String.valueOf(operacion.mult()), Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnDividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String num1 = txtNum1.getText().toString();
                String num2 = txtNum2.getText().toString();
                if(num1.trim().matches("") || num2.trim().matches(""))
                    Toast.makeText(getApplicationContext(), "Llena ambos campos", Toast.LENGTH_SHORT).show();
                else {
                    Operaciones operacion = new Operaciones(Float.valueOf(num1), Float.valueOf(num2));
                    Toast.makeText(getApplicationContext(), "Resultado: " + String.valueOf(operacion.div()), Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtNum1.setText("");
                txtNum2.setText("");
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
